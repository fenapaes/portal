<?php
Router::connect(
    '/portal', array(
		'plugin' => 'Portal',
		'controller' => 'Status',
		'action' => 'index'
	)
);
Router::connect(
    '/portal/:controller/:action/*', array('plugin' => 'Portal')
);
Router::connect(
    '/portal/:controller/*', array('plugin' => 'Portal')
);
