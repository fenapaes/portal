<?php

App::uses('Controller', 'Controller');

class PortalAppController extends AppController {
	
	public function beforeFilter() {
		
		parent::beforeFilter();

		// Carregar Layout bootstrap
		$this->layout = 'Portal.portal';

		// Gera lista de Menus
		$menus = $this->MenuPortal->generate();
		$this->set('menus', $menus);
		
		// Enviar dados do usuario para a view
		$this->set('usuario', $this->Auth->user());
		
		// Carrega dados do Site Selecionados
		if ($this->Session->check('selected_site')) {
			$this->set('selected_site', $this->Session->read('selected_site'));
			$this->selected_site = $this->Session->read('selected_site');
		}
		
		// Botoes padroes para listagem de dados
		// É possível sobre-escrever os botôes padrão criando outra variavel "listButtons" no controller ou na view
		$this->indexButtons = array(
		array(
			'text' => false,
			'title' => 'Editar',
			'action' => 'edit',
			'icon' => 'pencil'
		),
		array(
			'text' => false,
			'title' => 'Excluir',
			'action' => 'del',
			'icon' => 'trash',
			'method' => 'post',
			'message' => 'Tem Certeza?'
		)
	);
		$this->set('indexButtons', $this->indexButtons);
	
		// Ações padroes para listagem de dados
		// É possível sobre-escrever as açõs padrão criando outra variavel "actionButtons" no controller ou na view
		$this->set('indexActions', array(
			array(
				'style' => 'success',
				'text' => 'Adicionar',
				'title' => 'Adicionar',
				'action' => 'add',
				'icon' => 'plus'
			),
			array(
				'style' => 'info',
				'text' => 'Filtrar',
				'title' => 'Filtrar',
				'action' => 'filter',
				'icon' => 'filter'
			)
		));
		// Ações padroes para o formulario de dados
		$this->set('formActions', array(
			array(
				'style' => 'success',
				'text' => 'Gravar',
				'title' => 'Gravar',
				'icon' => 'floppy-disk',
				'submit' => true
			),
			array(
				'style' => 'default',
				'text' => 'Cancelar',
				'title' => 'Cancelar',
				'action' => 'index',
				'icon' => 'remove'
			)
		));
	}
}
