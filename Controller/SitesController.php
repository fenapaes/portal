<?php
class SitesController extends PortalAppController {
	
	public $uses = array( 'Portal.Site' );
	
	public function save($id = false) {
		if ($this->request->isPost()){
			$data = $this->request->data;
			if ($data['Site']['parent_id'] == 0) {
				unset($data['Site']['parent_id']);
			}
			if ($id) {
				$data['Site']['id'] = $id;
			}
			$this->Site->save($data);
			$this->Bootstrap->setFlash('Registro salvo com successo!');
			$this->redirect(array('action'=>'index'));
		};
	}
	
	public function related( $id = false ) {
		$Entidades = $this->Site->Entidade->find('list',array('fields'=>array('id','nome'),'limit'=>100));
		$this->set('Entidades',$Entidades);
		
		$Templates = $this->Site->Template->find('list',array('fields'=>array('id','nome')));
		$this->set('Templates',$Templates);
		
		$Estilos = $this->Site->Estilo->find('list',array('fields'=>array('id','nome')));
		$this->set('Estilos',$Estilos);
		
		$conditions = array(
			'Site.parent_id !=' => $id
		);
		$Sites = array('0'=>'Nenhum') + $this->Site->find('list',array('conditions'=>$conditions, 'fields'=>array('id','nome')));
		$this->set('Sites',$Sites);
	}
	
	public function index() {
		$usuario = $this->Auth->user();
		
		if ($usuario['Grupo']['admin']) {
			$indexButtons = $this->indexButtons;
			array_push($indexButtons, array(
				'text' => false,
				'title' => 'Selecionar',
				'icon' => 'eye-open',
				'action' => 'select'
			));
			$this->set('indexButtons', $indexButtons);
		}
		$this->set('title_for_layout','Sites');
		$this->Site->Behaviors->attach('Containable');
		$this->Site->contain(
			'Entidade'
		);
		
		$Sites = $this->Paginator->paginate('Site');
		$this->set('data', $Sites);
		
	}
	
	public function add() {
		$this->save();
		$this->related();
		$this->render('form');
	}
	
	public function edit( $site_id = false ) {
		$this->save($site_id);
		$this->related($site_id);
		$this->request->data = $this->Site->read(null, $site_id);
		$this->render('form');
	}
	
	public function del( $site_id = null ) {
		if ($this->request->isPost()) {
			$this->Site->delete($site_id);
			$this->Bootstrap->setFlash('Usuário excluido com successo!','success');
			$this->redirect(array('action'=>'index'));
		} else {
			$this->Bootstrap->setFlash('Erro na tentativa de excluir o Usuário!!','danger');
		}
	}
	
	public function filter() {
		$this->related();
		if ($this->Session->check('filterUsuario')) {
			$this->request->data = $this->Session->read('filterUsuario');
		}
	}
	
	public function filterApply() {
		$filters = $this->Session->read('filterUsuario');
		$conditions = array();
		foreach ($filters as $key=>$value) {
			$conditions[$key] = '%'.$value.'%';
		}
		return $conditions;
	}
	
	public function select($site_id = false) {
		if ($site_id) {
			$site = $this->Site->read(null, $site_id);
			$this->Session->write('selected_site', $site);
			$this->Bootstrap->setFlash('Site selecionado com successo!');
			$this->redirect(array('action'=>'index'));
		}
	}
}
