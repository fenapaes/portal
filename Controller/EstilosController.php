<?php
class EstilosController extends PortalAppController {
	
	public $uses = array( 'Portal.Estilo' );
	
	public function save($id = false) {
		if ($this->request->isPost()){
			$data = $this->request->data;
			
			if ($id) {
				$data['Estilo']['id'] = $id;
			}
			
			$this->Estilo->save($data);
			$this->Bootstrap->setFlash('Registro salvo com successo!');
			$this->redirect(array('action'=>'index'));
		};
	}
	
	public function related( $id = false ) {
		$Thumbs = $this->Estilo->Thumb->find('list',array('fields'=>array('id','nome')));
		$this->set('Thumbs', array('0'=>'Nenhum') + $Thumbs);
	}
	
	public function index() {
		$usuario = $this->Auth->user();
		
		$this->set('title_for_layout','Estilos');
		$this->Estilo->Behaviors->attach('Containable');
		$this->Estilo->contain();
		
		$Estilos = $this->Paginator->paginate('Estilo');
		$this->set('data', $Estilos);
		
	}
	
	public function add() {
		$this->save();
		$this->related();
		$this->render('form');
	}
	
	public function edit( $estilo_id = false ) {
		$this->save($estilo_id);
		$this->related($estilo_id);
		$this->request->data = $this->Estilo->read(null, $estilo_id);
		$this->render('form');
	}
	
	public function del( $estilo_id = null ) {
		if ($this->request->isPost()) {
			$this->Estilo->delete($estilo_id);
			$this->Bootstrap->setFlash('Registro excluido com successo!','success');
			$this->redirect(array('action'=>'index'));
		} else {
			$this->Bootstrap->setFlash('Erro na tentativa de excluir o Registro!!','danger');
		}
	}
	
	public function filter() {
		$this->related();
		if ($this->Session->check('filterEstilo')) {
			$this->request->data = $this->Session->read('filterEstilo');
		}
	}
	
	public function filterApply() {
		$filters = $this->Session->read('filterEstilo');
		$conditions = array();
		foreach ($filters as $key=>$value) {
			$conditions[$key] = '%'.$value.'%';
		}
		return $conditions;
	}

}

