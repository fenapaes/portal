<?php
class ThumbsController extends PortalAppController {
	
	public $uses = array( 'Portal.Thumb' );
	
	public function save($id = false) {
		if ($this->request->isPost()){
			$data = $this->request->data;
			if ($id) {
				$data['Thumb']['id'] = $id;
			}
			$site = $this->Session->read('selected_site');
			$data['Thumb']['site_id'] = $site['Site']['id'];
			$user = $this->Auth->user();
			$data['Thumbs']['usuario_id'] = $user['id'];
			
			$this->Thumb->save($data);
			$this->Bootstrap->setFlash('Registro salvo com successo!');
			$this->redirect(array('action'=>'index'));
		};
	}
	
	public function related( $id = false ) {
		$Imagens = $this->Thumb->Imagem->find('list',array('fields'=>array('id','nome')));
		$this->set('Imagens', $Imagens);
	}
	
	public function index() {
		$usuario = $this->Auth->user();
		
		$this->set('title_for_layout','Thumbs');
		$this->Thumb->Behaviors->attach('Containable');
		$this->Thumb->contain();
		
		$Thumbs = $this->Paginator->paginate('Thumb');
		$this->set('data', $Thumbs);
		
	}
	
	public function add() {
		$this->save();
		$this->related();
		$this->render('form');
	}
	
	public function edit( $thumbnail_id = false ) {
		$this->save($thumbnail_id);
		$this->related($thumbnail_id);
		$this->request->data = $this->Thumb->read(null, $thumbnail_id);
		$this->render('form');
	}
	
	public function del( $thumbnail_id = null ) {
		if ($this->request->isPost()) {
			$this->Thumb->delete($thumbnail_id);
			$this->Bootstrap->setFlash('Registro excluido com successo!','success');
			$this->redirect(array('action'=>'index'));
		} else {
			$this->Bootstrap->setFlash('Erro na tentativa de excluir o Registro!!','danger');
		}
	}
	
	public function filter() {
		$this->related();
		if ($this->Session->check('filterThumb')) {
			$this->request->data = $this->Session->read('filterThumb');
		}
	}
	
	public function filterApply() {
		$filters = $this->Session->read('filterThumb');
		$conditions = array();
		foreach ($filters as $key=>$value) {
			$conditions[$key] = '%'.$value.'%';
		}
		return $conditions;
	}

}

