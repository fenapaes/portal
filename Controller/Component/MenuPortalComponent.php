<?php
class MenuPortalComponent extends Component {
	
	var $PortalMenu = array(
		array(
			'Link' => array(
				'texto' => 'Status',
				'children' => array(
					array(
						'Link' => array(
							'texto' => 'Desktop',
							'plugin' => 'Portal',
							'controller' => 'Status',
							'action' => 'index'
						)
					)
				)
			)
		),
		array(
			'Link' => array(
				'texto' => 'Sites',
				'children' => array(
					array(
						'Link' => array(
							'texto' => 'Sites',
							'plugin' => 'Portal',
							'controller' => 'Sites',
							'action' => 'index',
						)
					),
					array(
						'Link' => array(
							'texto' => 'Templates',
							'plugin' => 'Portal',
							'controller' => 'Templates',
							'action' => 'index',
						)
					),
					array(
						'Link' => array(
							'texto' => 'Estilos',
							'plugin' => 'Portal',
							'controller' => 'Estilos',
							'action' => 'index',
						)
					)
				)
			)
		),
		array(
			'Link' => array(
				'texto' => 'Plugins',
				'children' => array(
					array(
						'Link' => array(
							'texto' => 'Banners',
							'plugin' => 'Portal',
							'controller' => 'Banners',
							'action' => 'index',
						)
					),
					array(
						'Link' => array(
							'texto' => 'Notícias',
							'plugin' => 'Portal',
							'controller' => 'Noticias',
							'action' => 'index',
						)
					),
					array(
						'Link' => array(
							'texto' => 'Páginas',
							'plugin' => 'Portal',
							'controller' => 'Paginas',
							'action' => 'index',
						)
					),
					array(
						'Link' => array(
							'texto' => 'Plugins',
							'plugin' => 'Portal',
							'controller' => 'Plugins',
							'action' => 'index',
						)
					),
					array(
						'Link' => array(
							'texto' => 'Eventos',
							'plugin' => 'Portal',
							'controller' => 'Eventos',
							'action' => 'index',
						)
					),
					array(
						'Link' => array(
							'texto' => 'Vídeos',
							'plugin' => 'Portal',
							'controller' => 'Videos',
							'action' => 'index',
						)
					)
				)
			)
		),
		array(
			'Link' => array(
				'texto' => 'Imagens',
				'children' => array(
					array(
						'Link' => array(
							'texto' => 'Imagens',
							'plugin' => 'Portal',
							'controller' => 'Imagens',
							'action' => 'index',
						)
					),
					array(
						'Link' => array(
							'texto' => 'Thumbnails',
							'plugin' => 'Portal',
							'controller' => 'Thumbs',
							'action' => 'index',
						)
					)
				)
			)
		)
	);
	
    public function generate() {
        return $this->PortalMenu;
    }
}