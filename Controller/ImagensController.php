<?php
class ImagensController extends PortalAppController {
	
	public $uses = array('Portal.Imagem');
	public $dir = 'imagens';
	
	public function beforeFilter() {
		parent::beforeFilter();
	}
	
	public function uploadImage($imagem) {
		if (!is_dir(WWW_ROOT.$this->dir.'/'.$this->selected_site['Site']['id'])) {
			mkdir(WWW_ROOT.$this->dir.'/'.$this->selected_site['Site']['id'], 0775, true);
		}
		copy ($imagem['imagem']['tmp_name'], WWW_ROOT.$this->dir.'/'.$this->selected_site['Site']['id'].'/'.filter_var($imagem['imagem']['name'], FILTER_SANITIZE_EMAIL));
	}
	
	public function save($id = false) {
		if ($this->request->isPost()){
			$data = $this->request->data;
			if ($id) {
				$data['Imagem']['id'] = $id;
			}
			//pr($data);
			if ($data['Imagem']) {
				echo 'ok';
				$this->uploadImage($data['Imagem']);
				$Imagem = $data['Imagem']['imagem'];
				$data['Imagem']['imagem'] = filter_var($Imagem['name'], FILTER_SANITIZE_EMAIL);
			}
			$this->Imagem->save($data);
			$this->Bootstrap->setFlash('Registro salvo com successo!');
			$this->redirect(array('action'=>'index'));
		};
	}
	
	public function related($id = 0) {
		$Sites = $this->Imagem->Site->find('list',array('fields'=>array('id','nome')));
		$this->set('Sites',$Sites);
		
		$Tipos = $this->Imagem->ImagensTipo->find('list',array('fields'=>array('id','nome')));
		$this->set('Tipos',$Tipos);
	}
	
	public function filter() {
		
	}
	
	public function index() {
		
		$indexButtons = $this->indexButtons;
		array_push($indexButtons, array(
			'text' => false,
			'title' => 'Ver',
			'icon' => 'eye-open',
			'action' => 'view'
		));
		$this->set('indexButtons', $indexButtons);
		
		$this->set('title_for_layout','Imagens');
		$this->Imagem->Behaviors->attach('Containable');
		$this->Imagem->contain();
		
		$Imagens = $this->Paginator->paginate('Imagem');
		$this->set('data', $Imagens);
		
	}
	
	public function add() {
		$this->save();
		$this->related();
		$this->render('form');
	}
	
	public function edit($imagem_id = null) {
		$this->save($imagem_id);
		$this->related($imagem_id);
		$this->request->data = $this->Imagem->read(null, $imagem_id);
		$this->render('form');
	}
	
	public function del( $imagem_id = null ) {
		if ($this->request->isPost()) {
			$this->Imagem->delete($imagem_id);
			$this->Bootstrap->setFlash('Registro excluido com successo!','success');
			$this->redirect(array('action'=>'index'));
		} else {
			$this->Bootstrap->setFlash('Erro na tentativa de excluir o Usuário!!','danger');
		}
	}
	
	public function view ( $imagem_id = null ) {
		$this->Imagem->Behaviors->attach('Containable');
		$this->Imagem->contain();
		$this->set('Imagem', $this->Imagem->read(null, $imagem_id));
	}
}

