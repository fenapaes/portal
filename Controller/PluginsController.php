<?php
class PluginsController extends PortalAppController {

    public function related() {
        
    }
	public function index() {
		
		$this->set('title_for_layout','Plugins');
		$this->Plugin->Behaviors->attach('Containable');
		$this->Plugin->contain();
		
		$Plugins = $this->Paginator->paginate('Plugin');
		$this->set('data', $Plugins);
		
	}
    
    public function add($plugin_id = null) {
		if ($this->request->isPost()){
			$data = $this->request->data;
			$this->Plugin->save($data);
			$this->redirect(array('action'=>'index'));
		};
		$this->related();
		$this->render('form');
	}
}