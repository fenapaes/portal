<?php
class NoticiasController extends PortalAppController {
	
	public $uses = array( 'Portal.Noticia' );
	
	public function save($id = false) {
		if ($this->request->isPost()){
			$data = $this->request->data;
			
			if ($id) {
				$data['Noticia']['id'] = $id;
			}
			$site = $this->Session->read('selected_site');
			$data['Noticia']['site_id'] = $site['Site']['id'];
			$user = $this->Auth->user();
			$data['Noticia']['usuario_id'] = $user['id'];
			
			//$data['Noticia']['corpo'] = htmlentities($data['Noticia']['corpo']);
			
			$this->Noticia->save($data);
			$this->Bootstrap->setFlash('Registro salvo com successo!');
			$this->redirect(array('action'=>'index'));
		};
	}
	
	public function related( $id = false ) {
		//$Sites = $this->Noticia->Site->find('list',array('fields'=>array('id','nome')));
		//$this->set('Sites', $Sites);
		
		$Thumbs = $this->Noticia->Thumb->find('list',array('fields'=>array('id','nome')));
		$this->set('Thumbs', $Thumbs);
		
		$Assuntos = $this->Noticia->Assunto->find('list',array('fields'=>array('id','nome')));
		$this->set('Assuntos', $Assuntos);
	}
	
	public function index() {
		$usuario = $this->Auth->user();
		
		$this->set('title_for_layout','Notícias');
		$this->Noticia->Behaviors->attach('Containable');
		$this->Noticia->contain(
			'Assunto'
		);
		
		$Noticias = $this->Paginator->paginate('Noticia');
		$this->set('data', $Noticias);
		
	}
	
	public function add() {
		$this->save();
		$this->related();
		$this->render('form');
	}
	
	public function edit( $noticia_id = false ) {
		$this->save($noticia_id);
		$this->related($noticia_id);
		$this->request->data = $this->Noticia->read(null, $noticia_id);
		$this->render('form');
	}
	
	public function del( $noticia_id = null ) {
		if ($this->request->isPost()) {
			$this->Noticia->delete($noticia_id);
			$this->Bootstrap->setFlash('Registro excluido com successo!','success');
			$this->redirect(array('action'=>'index'));
		} else {
			$this->Bootstrap->setFlash('Erro na tentativa de excluir o Registro!!','danger');
		}
	}
	
	public function filter() {
		$this->related();
		if ($this->Session->check('filterNoticia')) {
			$this->request->data = $this->Session->read('filterNoticia');
		}
	}
	
	public function filterApply() {
		$filters = $this->Session->read('filterNoticia');
		$conditions = array();
		foreach ($filters as $key=>$value) {
			$conditions[$key] = '%'.$value.'%';
		}
		return $conditions;
	}

}

