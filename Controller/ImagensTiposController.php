<?php
class ImagensTiposController extends PortalAppController {
	
	public $uses = array('Portal.ImagensTipo');
	public $dir = '/imagens/';
	
	public function beforeFilter() {
		parent::beforeFilter();
	}
	
	public function save($id = false) {
		if ($this->request->isPost()){
			$data = $this->request->data;
			if ($id) {
				$data['ImagensTipo']['id'] = $id;
			}
			$this->ImagensTipo->save($data);
			$this->Bootstrap->setFlash('Registro salvo com successo!');
			$this->redirect(array('action'=>'index'));
		};
	}
	
	public function related($id = 0) {
		
	}
	
	public function filter() {
		
	}
	
	public function index() {
		
		$this->set('title_for_layout','Tipos de Imagens');
		$this->ImagensTipo->Behaviors->attach('Containable');
		$this->ImagensTipo->contain();
		
		$ImagensTipos = $this->Paginator->paginate('ImagensTipo');
		$this->set('data', $ImagensTipos);
		
	}
	
	public function add($imagens_tipo_id = null) {
		$this->save($imagens_tipo_id);
		$this->related();
		$this->render('form');
	}
	
	public function edit($imagens_tipo_id = null) {
		$this->save($imagens_tipo_id);
		$this->related($imagens_tipo_id);
		$this->request->data = $this->ImagensTipo->read(null, $imagens_tipo_id);
		$this->render('form');
	}
	
	public function del( $imagens_tipo_id = null ) {
		if ($this->request->isPost()) {
			$this->ImagensTipo->delete($imagens_tipo_id);
			$this->Bootstrap->setFlash('Registro excluido com successo!','success');
			$this->redirect(array('action'=>'index'));
		} else {
			$this->Bootstrap->setFlash('Erro na tentativa de excluir o Usuário!!','danger');
		}
	}
}

