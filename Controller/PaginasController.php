<?php
class PaginasController extends PortalAppController {
	
	public $uses = array( 'Portal.Pagina' );
	
	public function save($id = false) {
		if ($this->request->isPost()){
			$data = $this->request->data;
			
			if ($id) {
				$data['Pagina']['id'] = $id;
			}
			$site = $this->Session->read('selected_site');
			$data['Pagina']['site_id'] = $site['Site']['id'];
			$user = $this->Auth->user();
			$data['Pagina']['usuario_id'] = $user['id'];
			
			$this->Pagina->save($data);
			$this->Bootstrap->setFlash('Registro salvo com successo!');
			$this->redirect(array('action'=>'index'));
		};
	}
	
	public function related( $id = false ) {
		$Sites = $this->Pagina->Site->find('list',array('fields'=>array('id','nome')));
		$this->set('Sites', $Sites);
		
		$Paginas = array('0'=>'Nenhuma') + $this->Pagina->find('list',array('fields'=>array('id','nome'),'conditions'=>array('Pagina.pagina_id'=>'NULL')));
		$this->set('Paginas', $Paginas);
		
	}
	
	public function index() {
		$usuario = $this->Auth->user();
		$this->set('title_for_layout','Páginas');
		$this->Pagina->Behaviors->attach('Containable');
		$this->Pagina->contain();
		
		$Paginas = $this->Paginator->paginate('Pagina');
		$this->set('data', $Paginas);
		
	}
	
	public function add() {
		$this->save();
		$this->related();
		$this->render('form');
	}
	
	public function edit( $pagina_id = false ) {
		$this->save($pagina_id);
		$this->related($pagina_id);
		$this->request->data = $this->Pagina->read(null, $pagina_id);
		$this->render('form');
	}
	
	public function del( $pagina_id = null ) {
		if ($this->request->isPost()) {
			$this->Pagina->delete($pagina_id);
			$this->Bootstrap->setFlash('Registro excluido com successo!','success');
			$this->redirect(array('action'=>'index'));
		} else {
			$this->Bootstrap->setFlash('Erro na tentativa de excluir o Registro!!','danger');
		}
	}
	
	public function filter() {
		$this->related();
		if ($this->Session->check('filterPagina')) {
			$this->request->data = $this->Session->read('filterPagina');
		}
	}
	
	public function filterApply() {
		$filters = $this->Session->read('filterPagina');
		$conditions = array();
		foreach ($filters as $key=>$value) {
			$conditions[$key] = '%'.$value.'%';
		}
		return $conditions;
	}

}

