<?php
class BannersController extends PortalAppController {
	
	public $uses = array( 'Portal.Banner' );
	
	public function save($id = false) {
		if ($this->request->isPost()){
			$data = $this->request->data;
			
			if ($id) {
				$data['Banner']['id'] = $id;
			}
			$site = $this->Session->read('selected_site');
			$data['Banner']['site_id'] = $site['Site']['id'];
			$user = $this->Auth->user();
			$data['Banner']['usuario_id'] = $user['id'];
			
			$this->Banner->save($data);
			$this->Bootstrap->setFlash('Registro salvo com successo!');
			$this->redirect(array('action'=>'index'));
		};
	}
	
	public function related( $id = false ) {
		$Thumbs = $this->Banner->Thumb->find('list',array('fields'=>array('id','nome')));
		$this->set('Thumbs', $Thumbs);
		
		$BannersPosicoes = $this->Banner->BannersPosicao->find('list',array('fields'=>array('id','nome')));
		$this->set('BannersPosicoes', $BannersPosicoes);
	}
	
	public function index() {
		$usuario = $this->Auth->user();
		
		$this->set('title_for_layout','Banners');
		$this->Banner->Behaviors->attach('Containable');
		$this->Banner->contain();
		
		$Banners = $this->Paginator->paginate('Banner');
				$this->set('data', $Banners);
		
	}
	
	public function add() {
		$this->save();
		$this->related();
		$this->render('form');
	}
	
	public function edit( $banner_id = false ) {
		$this->save($banner_id);
		$this->related($banner_id);
		$this->request->data = $this->Banner->read(null, $banner_id);
		$this->render('form');
	}
	
	public function del( $banner_id = null ) {
		if ($this->request->isPost()) {
			$this->Banner->delete($banner_id);
			$this->Bootstrap->setFlash('Registro excluido com successo!','success');
			$this->redirect(array('action'=>'index'));
		} else {
			$this->Bootstrap->setFlash('Erro na tentativa de excluir o Registro!!','danger');
		}
	}
	
	public function filter() {
		$this->related();
		if ($this->Session->check('filterBanner')) {
			$this->request->data = $this->Session->read('filterBanner');
		}
	}
	
	public function filterApply() {
		$filters = $this->Session->read('filterBanner');
		$conditions = array();
		foreach ($filters as $key=>$value) {
			$conditions[$key] = '%'.$value.'%';
		}
		return $conditions;
	}

}

