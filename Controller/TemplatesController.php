<?php
class TemplatesController extends PortalAppController {
	
	public $uses = array('Portal.Template');
	
	public function related() {
		$Estilos = $this->Template->Estilo->find('list', array('fields'=>array('id','nome')));
		$this->set('Estilos',$Estilos);
	}
	
	public function index() {
		
		$this->set('title_for_layout','Templates');
		$this->Template->Behaviors->attach('Containable');
		$this->Template->contain();
		
		$Templates = $this->Paginator->paginate('Template');
		$this->set('data', $Templates);
		
	}
	
	public function add() {
		if ($this->request->isPost()){
			$this->Template->save($this->request->data);
			$this->Bootstrap->setFlash('Registro salvo com successo!');
			$this->redirect(array('action'=>'index'));
		};
		$this->related();
		$this->render('form');
	}
	
	public function edit($template_id = null) {
		if ($this->request->isPost()){
			$data = $this->request->data;
			$data['Template']['id'] = $template_id;
			$this->Template->save($data);
			$this->Bootstrap->setFlash('Registro salvo com successo!');
			$this->redirect(array('action'=>'index'));
		};
		$this->related();
		$this->request->data = $this->Template->read(null, $template_id);
		$this->render('form');
	}
	
	public function del( $template_id = null ) {
		if ($this->Template->delete($template_id)) {
		$this->Bootstrap->setFlash('Registro excluído com successo!');
		$this->redirect(array('action'=>'index'));
		} else {
			$this->Bootstrap->setFlash('Erro ao excluir o registro!','danger');
			//$this->redirect(array('action'=>'index'));
		}
	}
}
