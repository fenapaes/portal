<?php
App::uses('AppModel', 'Model');

class Pagina extends PortalAppModel {
	//public $useDbConfig = 'sistemas';
	public $displayField = 'titulo';
    public $useTable = 'paginas';
	
	public $belongsTo = array(
		'Site'
	);
}
