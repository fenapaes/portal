<?php
App::uses('AppModel', 'Model');
class Template extends PortalAppModel {
	//public $useDbConfig = 'sistemas';
	public $displayField = 'nome';
	
	public $belongsTo = array(
		'Estilo'						  
	);
	
	public function beforeDelete($cascade = true) {
		if (!$cascade) {
			return true;
		} else {
			return false;
		}
	}
	
}