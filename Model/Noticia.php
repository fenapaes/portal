<?php
App::uses('AppModel', 'Model');

class Noticia extends PortalAppModel {
	//public $useDbConfig = 'sistemas';
	public $displayField = 'titulo';
    public $useTable = 'noticias';
	
	public $belongsTo = array(
		'Portal.Site',
		'Portal.Thumb',
		'Portal.Assunto'
	);
	
	public function aftedrFind($results, $primary = false) {
		foreach ($results as $key => $val) {
		        if (isset($val['Noticia']['data_noticia'])) {
		            $results[$key]['Noticia']['data_noticia'] = $this->dateFormatAfterFind(
		                $val['Noticia']['data_noticia']
		            );
		        }
		        if (isset($val['Noticia']['data_publicacao'])) {
		            $results[$key]['Noticia']['data_publicacao'] = $this->dateFormatAfterFind(
		                $val['Noticia']['data_publicacao']
		            );
		        }
		        if (isset($val['Noticia']['data_revogacao'])) {
		            $results[$key]['Noticia']['data_revogacao'] = $this->dateFormatAfterFind(
		                $val['Noticia']['data_revogacao']
		            );
		        }
		}
		return $results;
	}
	
	public function befossreSave($results, $primary = false) {
		foreach ($results as $key => $val) {
		        if (isset($val['Noticia']['data_noticia'])) {
		            $results[$key]['Noticia']['data_noticia'] = $this->dateFormatAfterFind(
		                $val['Noticia']['data_noticia']
		            );
		        }
		}
		return $results;
	}
	
	public function dateFormatAfterFind($dateString) {
	    return date('d/m/Y', strtotime($dateString));
	}
	
	public function dateFormatBeforeSave($dateString) {
	    return date('Y-m-d', date_create_from_format('d/m/Y',$dateString));
	}
}
