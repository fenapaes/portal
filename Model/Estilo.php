<?php
App::uses('AppModel', 'Model');
class Estilo extends PortalAppModel {
	//public $useDbConfig = 'sistemas';
	public $displayField = 'nome';
	
	public $belongsTo = array(
		'Thumb'
	);
	
	public function beforeDelete($cascade = true) {
		if (!$cascade) {
			return true;
		} else {
			return false;
		}
	}
	
}