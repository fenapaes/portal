<?php
App::uses('AppModel', 'Model');

class Imagem extends PortalAppModel {
	//public $useDbConfig = 'sistemas';
	public $displayField = 'nome';
    public $useTable = 'imagens';
	
	public $belongsTo = array(
		'Site',
		'ImagensTipo'
	);
}
