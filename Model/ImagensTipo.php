<?php
App::uses('AppModel', 'Model');

class ImagensTipo extends PortalAppModel {
	//public $useDbConfig = 'sistemas';
	public $displayField = 'nome';
    public $useTable = 'imagens_tipos';
	
	public $hasMany = array(
		'Imagem'
	);
}
