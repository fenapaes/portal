<?php
App::uses('AppModel', 'Model');

class Banner extends PortalAppModel {
	//public $useDbConfig = 'sistemas';
	public $displayField = 'nome';
    public $useTable = 'banners';
	
	public $belongsTo = array(
		'Site',
		'Thumb',
		'BannersPosicao'
	);
}
