<?php
App::uses('AppModel', 'Model');

class Thumb extends PortalAppModel {
	//public $useDbConfig = 'sistemas';
	public $displayField = 'nome';
    public $useTable = 'thumbs';
	
	public $belongsTo = array(
		'Imagem'	
	);
	
	public $hasMany = array(
		'Noticia'
	);
}
