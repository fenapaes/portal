<?php
App::uses('AppModel', 'Model');

class Entidade extends PortalAppModel {
	public $useDbConfig = 'Entidades';
	public $displayField = 'nome';
    public $useTable = 'entidades';
	
	public $belongsTo = array(
		'Site'
	);
}
