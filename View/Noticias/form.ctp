<?php $this->extend('Bootstrap./Common/form'); ?>
<?php $this->assign('panelStyle','primary'); // Estilo do painel da página ( 'default' como padrao ) ?>

<?php $this->assign('pageHeader','Notícias'); ?>

<?php $this->start('actions');
    echo $this->Bootstrap->actions(null, $formActions);
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create('Noticia', array('type'=>'POST'));
$this->end();
$this->start('form-body');
	echo $this->Bootstrap->input('titulo');
    echo $this->Bootstrap->input('assunto_id', array('options'=>$Assuntos));
	echo $this->Bootstrap->input('slug');
	echo $this->Bootstrap->input('corpo', array('rows'=>20, 'class'=>'ckeditor'));
	
    echo $this->Bootstrap->input('data_noticia',array('type'=>'text','class'=>'form-control datepicker'));
    echo $this->Bootstrap->input('data_publicacao',array('type'=>'text','class'=>'form-control datepicker'));
    echo $this->Bootstrap->input('data_revogacao',array('type'=>'text','class'=>'form-control datepicker'));
    
    //echo $this->Bootstrap->input('site_id', array('options'=>$Sites));
    echo $this->Bootstrap->input('thumb_id', array('options'=>$Thumbs));
$this->end();
?>
