<?php echo $this->Bootstrap->pageHeader('Status'); ?>

<div class="row">
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Alterações</h3>
            </div>
            <div class="panel-body">
                Modificações feitas pelos usuários
            </div>
            <div class="panel-footer">
                Rodapé
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Mensagens</h3>
            </div>
            <div class="panel-body">
                Mensagem da Rede
            </div>
            <div class="panel-footer">
                Rodapé
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Agenda</h3>
            </div>
            <div class="panel-body">
                Agenda e Eventos
            </div>
            <div class="panel-footer">
                Rodapé
            </div>
        </div>
    </div>
</div>