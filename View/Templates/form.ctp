<?php $this->extend('Bootstrap./Common/form'); ?>
<?php $this->assign('panelStyle','primary'); // Estilo do painel da página ( 'default' como padrao ) ?>

<?php $this->assign('pageHeader','Templates'); ?>

<?php $this->start('actions');
    echo $this->Bootstrap->actions(null, $formActions);
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create('Template', array('type'=>'POST'));
$this->end();
$this->start('form-body');
	echo $this->Bootstrap->input('nome');
	echo $this->Bootstrap->input('estilo_id', array('label'=>'Estilo','options'=>$Estilos));
$this->end();
?>
