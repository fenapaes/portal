<?php $this->extend('Bootstrap./Common/form'); ?>
<?php $this->assign('panelStyle','primary'); // Estilo do painel da página ( 'default' como padrao ) ?>

<?php $this->assign('pageHeader','Sites'); ?>

<?php $this->start('actions');
    echo $this->Bootstrap->actions(null, $formActions);
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create('Site', array('type'=>'POST'));
$this->end();
$this->start('form-body');
	echo $this->Bootstrap->input('nome');
	echo $this->Bootstrap->input('dominio');
	echo $this->Bootstrap->input('entidade_id',array('options'=>$Entidades));
	echo $this->Bootstrap->input('logo');
	echo $this->Bootstrap->input('template_id',array('options'=>$Templates));
	echo $this->Bootstrap->input('estilo_id',array('options'=>$Estilos));
	echo $this->Bootstrap->input('parent_id',array('label'=>'Site Pai','options'=>$Sites));
$this->end();
?>
