<?php $this->extend('Bootstrap./Common/form'); ?>
<?php $this->assign('panelStyle','primary'); // Estilo do painel da página ( 'default' como padrao ) ?>

<?php $this->assign('pageHeader','Banners'); ?>

<?php $this->start('actions');
    echo $this->Bootstrap->actions(null, $formActions);
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create('Banner', array('type'=>'POST'));
$this->end();
$this->start('form-body');
	echo $this->Bootstrap->input('nome');
    echo $this->Bootstrap->input('ordem');
    echo $this->Bootstrap->input('url');
    echo $this->Bootstrap->input('data_publicacao',array('type'=>'text'));
    echo $this->Bootstrap->input('data_revogacao',array('type'=>'text'));
    echo $this->Bootstrap->input('clicks');
	echo $this->Bootstrap->input('thumb_id', array('options'=>$Thumbs));
	echo $this->Bootstrap->input('banners_posicao_id', array('options'=>$BannersPosicoes));
$this->end();
?>
