<?php
	$this->extend('Bootstrap./Common/index'); // Extend index padrao
	$this->assign('pageHeader','Banners'); // Header da página
	$this->assign('panelStyle','primary'); // Estilo do painel da página ( 'default' como padrao )
?>

<?php $this->start('actions');
	echo $this->Bootstrap->actions(null, $indexActions);
$this->end(); ?>

<?php $this->start('table-tr'); ?>
	<?php // Cabecalho da tabela para a listagem ?>
	<tr class="active">
		<th class="col-md-2">&nbsp;</th>
		<th><?php echo $this->Paginator->sort('nome','Nome');?></th>
		<th><?php echo $this->Paginator->sort('url','Url');?></th>
		<th><?php echo $this->Paginator->sort('clicks','Cliques');?></th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
<?php // Corpo da tabela para a listagem ?>
<?php foreach ($data as $Banner) { ?>
	<tr>
		<td><?php echo $this->Bootstrap->actions($Banner['Banner']['id'], $indexButtons); ?></td>
		<td><?php echo $Banner['Banner']['nome']; ?></td>
		<td><?php echo $Banner['Banner']['url']; ?></td>
		<td><?php echo $Banner['Banner']['clicks']; ?></td>
	</tr>
<?php } ?>
<?php $this->end(); ?>

