<?php $this->extend('Bootstrap./Common/form'); ?>
<?php $this->assign('panelStyle','primary'); // Estilo do painel da página ( 'default' como padrao ) ?>
<?php $this->assign('pageHeader','Imagens'); ?>

<?php $this->start('actions');
    echo $this->Bootstrap->actions(null, $formActions);
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create('Imagem', array('type'=>'file'));
$this->end();
$this->start('form-body');
	echo $this->Bootstrap->input('nome');
	echo $this->Bootstrap->input('site_id',array('label'=>'Site','options'=>$Sites));
	echo $this->Bootstrap->input('imagens_tipo_id', array('options'=>$Tipos));
	$imagem = (isset($this->data['Imagem']['imagem']))?('[ '.$this->data['Imagem']['imagem'].' ]'):('');
    echo $this->Bootstrap->input('imagem', array('type'=>'file','label'=>'Imagem '.$imagem ));
$this->end();
?>
