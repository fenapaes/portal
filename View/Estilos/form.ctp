<?php $this->extend('Bootstrap./Common/form'); ?>
<?php $this->assign('panelStyle','primary'); // Estilo do painel da página ( 'default' como padrao ) ?>
<?php $this->assign('pageHeader','Estilos'); ?>

<?php $this->start('actions');
    echo $this->Bootstrap->actions(null, $formActions);
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create('Estilo', array('type'=>'POST'));
$this->end();
$this->start('form-body');
	echo $this->Bootstrap->input('nome');
	echo $this->Bootstrap->input('cor_fundo', array('label'=>'Cor do Fundo'));
	echo $this->Bootstrap->input('cor_principal', array('label'=>'Cor Principal'));
	echo $this->Bootstrap->input('cor_secundaria', array('label'=>'Cor Secundária'));
	echo $this->Bootstrap->input('cor_terciaria', array('label'=>'Cor Terciária'));
	echo $this->Bootstrap->input('thumb_id', array('options'=>$Thumbs, 'label'=>'Imagem de Fundo'));
	echo $this->Bootstrap->input('imagem_fundo_posicao', array('label'=>'Posição da Imagem de Fundo'));
	echo $this->Bootstrap->input('imagem_fundo_scroll', array('label'=>'Scroll da Imagem de Fundo'));
	echo $this->Bootstrap->input('posicao_menu_lateral', array('label'=>'Posição de Menu Lateral'));
	
$this->end();
?>
