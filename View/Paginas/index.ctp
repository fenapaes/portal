<?php
	$this->extend('Bootstrap./Common/index'); // Extend index padrao
	$this->assign('pageHeader','Páginas'); // Header da página
	$this->assign('panelStyle','primary'); // Estilo do painel da página ( 'default' como padrao )
?>

<?php $this->start('actions');
	echo $this->Bootstrap->actions(null, $indexActions);
$this->end(); ?>

<?php $this->start('table-tr'); ?>
	<?php // Cabecalho da tabela para a listagem ?>
	<tr class="active">
		<th class="col-md-2">&nbsp;</th>
		<th><?php echo $this->Paginator->sort('title','Título');?></th>
		<th><?php echo $this->Paginator->sort('data_publicacao','Data');?></th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
<?php // Corpo da tabela para a listagem ?>
<?php foreach ($data as $Pagina) { ?>
	<tr>
		<td><?php echo $this->Bootstrap->actions($Pagina['Pagina']['id'], $indexButtons); ?></td>
		<td><?php echo $Pagina['Pagina']['nome']; ?></td>
		<td><?php echo $Pagina['Pagina']['data_publicacao']; ?></td>
	</tr>
<?php } ?>
<?php $this->end(); ?>

