<?php $this->extend('Bootstrap./Common/form'); ?>
<?php $this->assign('panelStyle','primary'); // Estilo do painel da página ( 'default' como padrao ) ?>

<?php $this->assign('pageHeader','Páginas'); ?>

<?php $this->start('actions');
    echo $this->Bootstrap->actions(null, $formActions);
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create('Pagina', array('type'=>'POST'));
$this->end();
$this->start('form-body');
	echo $this->Bootstrap->input('nome');
	echo $this->Bootstrap->input('menu',array('type'=>'text'));
	echo $this->Bootstrap->input('slug');
	echo $this->Bootstrap->input('corpo');
	
    echo $this->Bootstrap->input('data_noticia',array('type'=>'text'));
    echo $this->Bootstrap->input('data_publicacao',array('type'=>'text'));
    echo $this->Bootstrap->input('data_revogacao',array('type'=>'text'));
    
    echo $this->Bootstrap->input('site_id', array('options'=>$Sites));
	
	 echo $this->Bootstrap->input('pagina_id', array('options'=>$Paginas));
	 echo $this->Bootstrap->input('ordem');
$this->end();
?>
