<?php $this->extend('Bootstrap./Common/form'); ?>
<?php $this->assign('panelStyle','primary'); // Estilo do painel da página ( 'default' como padrao ) ?>

<?php $this->assign('pageHeader','Imagens'); ?>

<?php $this->start('actions');
    echo $this->Bootstrap->actions(null, $formActions);
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create('Thumb', array('type'=>'POST'));
$this->end();
$this->start('form-body');
	echo $this->Bootstrap->input('nome');
    echo $this->Bootstrap->input('largura');
	echo $this->Bootstrap->input('altura');
	echo $this->Bootstrap->input('formato');
	
    echo $this->Bootstrap->input('imagem_id', array('options'=>$Imagens));
$this->end();
?>
