<?php
	$this->extend('Bootstrap./Common/index'); // Extend index padrao
	$this->assign('pageHeader','Thumbs'); // Header da página
	$this->assign('panelStyle','primary'); // Estilo do painel da página ( 'default' como padrao )
?>

<?php $this->start('actions');
	echo $this->Bootstrap->actions(null, $indexActions);
$this->end(); ?>

<?php $this->start('table-tr'); ?>
	<?php // Cabecalho da tabela para a listagem ?>
	<tr class="active">
		<th class="col-md-2">&nbsp;</th>
		<th><?php echo $this->Paginator->sort('nome','Nome');?></th>
		<th><?php echo $this->Paginator->sort('largura','Largura');?></th>
		<th><?php echo $this->Paginator->sort('altura','Altura');?></th>
		<th><?php echo $this->Paginator->sort('formato','Formato');?></th>
	</tr>
<?php $this->end(); ?>

<?php $this->start('table-body'); ?>
<?php // Corpo da tabela para a listagem ?>
<?php foreach ($data as $Thumb) { ?>
	<tr>
		<td><?php echo $this->Bootstrap->actions($Thumb['Thumb']['id'], $indexButtons); ?></td>
		<td><?php echo $Thumb['Thumb']['nome']; ?></td>
		<td><?php echo $Thumb['Thumb']['largura']; ?></td>
		<td><?php echo $Thumb['Thumb']['altura']; ?></td>
		<td><?php echo $Thumb['Thumb']['formato']; ?></td>
	</tr>
<?php } ?>
<?php $this->end(); ?>

