<?php $this->extend('Bootstrap./Common/form'); ?>

<?php $this->start('pageHeader'); ?>Plugins<?php $this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create('Plugin', array('type'=>'POST'));
$this->end();
$this->start('form-body');
	echo $this->Bootstrap->input('nome');
    echo $this->Bootstrap->input('descricao');
    echo $this->Bootstrap->input('tabela');
    echo $this->Bootstrap->input('min_width');
    echo $this->Bootstrap->input('min_height');
    echo $this->Bootstrap->input('parametros');
$this->end();
?>
