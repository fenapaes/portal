<?php $this->extend('Bootstrap./Common/index'); ?>

<?php $this->start('pageHeader'); ?>Plugins<?php $this->end(); ?>

<?php $this->start('table-tr'); ?>
	<tr class="active">
		<th class="col-md-2">&nbsp;</th>
		<th class="col-md-4">Nome</th>
	</tr>
<?php $this->end(); ?>
<?php $this->start('table-body'); ?>
<?php foreach ($data as $Plugin) { ?>
	<tr>
		<td>
			<?php echo $this->Bootstrap->basicActions($Plugin['Plugin']['id']); ?>
		</td>
		<td><?php echo $Plugin['Plugin']['nome']; ?></td>
		
	</tr>
<?php } ?>
<?php $this->end(); ?>
