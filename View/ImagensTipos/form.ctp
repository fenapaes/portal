<?php $this->extend('Bootstrap./Common/form'); ?>
<?php $this->assign('panelStyle','primary'); // Estilo do painel da página ( 'default' como padrao ) ?>
<?php $this->assign('pageHeader','Tipos de Imagens'); ?>

<?php $this->start('actions');
    echo $this->Bootstrap->actions(null, $formActions);
$this->end(); ?>

<?php $this->start('form-create');
echo $this->Bootstrap->create('ImagensTipo', array('type'=>'post'));
$this->end();
$this->start('form-body');
	echo $this->Bootstrap->input('nome');
$this->end();
?>
