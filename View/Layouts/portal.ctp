<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php echo $this->Html->charset(); ?>
        <title><?php echo $title_for_layout; ?></title>
        <?php
        echo $this->Html->meta( 'icon' );

        // jQuery
        echo $this->Html->script( 'Bootstrap./js/jquery-2.1.0.min');
        // MaskedInput
        echo $this->Html->script( 'Bootstrap.jquery.inputmask.bundle.min' );
        // HotKeus
        echo $this->Html->script( 'Bootstrap.jquery.hotkeys' );
		// CKEditor
		echo $this->Html->script( 'Portal./ckeditor/ckeditor.js');
		// Bootstrap Datepicker
		echo $this->Html->script( 'Portal.datepicker/bootstrap-datepicker.js');
		echo $this->Html->script( 'Portal.datepicker/locales/bootstrap-datepicker.pt-BR.js');
		
        // Bootstrap
        echo $this->Html->css( 'Bootstrap./bootstrap/css/lumen.min' );
        echo $this->Html->script( 'Bootstrap./bootstrap/js/bootstrap.min' );

        echo $this->fetch( 'meta' );
        echo $this->fetch( 'css' );
        echo $this->fetch( 'script' );

        // AuthBootstrap
        echo $this->Html->css('Bootstrap.cakeboot');
        echo $this->Html->script( 'Bootstrap.authbootstrap' );
        ?>
    </head>
    <body>
        <?php 
   	        if (isset($menus)) {
       	        echo $this->Element( 'Portal.navbar-top', array('usuario',$usuario) ); 
			}
		?>
        <div class="container">
            <?php echo $this->Session->flash( 'flash', array( 'element'=>'Bootstrap.flash' ) ); ?>
       	 	<?php echo $this->fetch( 'content' ); ?>
        </div>
        <?php echo $this->element('sql_dump'); ?>
    </body>
</html>
